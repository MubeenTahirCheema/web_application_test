﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplicationTest.DTOs;
using WebApplicationTest.Model;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplicationTest
{
    //[Produces("application/json")]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly DBContaxt db;
        private readonly IMapper mapper;
        public UserController(DBContaxt contaxt, IMapper mapper)
        {
            this.db = contaxt;
            this.mapper = mapper;
        }
        [Route("~/api/user")]
        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<UserDTO> Get()
        {
            var user = db.User.ToList();
            if (user.Count > 0)
                return mapper.Map<List<UserDTO>>(user);
            else
                return null;
        }

        [Route("~/api/user")]
        // GET api/<controller>/5
        [HttpGet("{email}")]
        public UserDTO Get(string email)
        {
            var user = db.User.Where(a => a.Email.ToLower().CompareTo(email.ToLower()) == 0).ToList();
            if (user.Count > 0)
                return mapper.Map<UserDTO>(user.First());
            else
                return null;
        }

        // POST api/<controller>
        [Route("~/api/user")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]RegisterUserRequest value)
        {
            if (ModelState.IsValid)
            {
                Model.User user = new User();
                user = mapper.Map<RegisterUserRequest, User>(value, opt => opt.ConfigureMap(MemberList.None));
                db.User.Add(user);
                db.SaveChangesAsync();
                return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
