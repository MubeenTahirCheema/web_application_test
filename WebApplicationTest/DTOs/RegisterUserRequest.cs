﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DTOs
{
    public class RegisterUserRequest
    {
        [Required]
        [MinLength(3, ErrorMessage="`Name` must have at-least 3 characters")]
        public string Name { get; set; }

        [Required]
        [RegularExpression(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$", ErrorMessage = "Invalid Email")]
        public string Email { get; set; }

        [Required]
        [MinLength(6, ErrorMessage = "Password length must be at-least 6 charecters")]
        [MaxLength(20, ErrorMessage = "Password length must not be more then 20 charecters")]
        public string Password { get; set; }
    }
}
