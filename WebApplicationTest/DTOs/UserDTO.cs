﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DTOs
{
    public class UserDTO
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}
